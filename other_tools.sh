#!/bin/bash

basedir=$(dirname "$(readlink -f "$0")")

prefix=/opt/libraries

## CMake

cmake_version="3.21.3"
cmake_version_parts=(${cmake_version//./ })
cmake_maj_minor="${cmake_version_parts[0]}.${cmake_version_parts[1]}"

echo "Install CMake $cmake_version"

wget -nv "https://cmake.org/files/v$cmake_maj_minor/cmake-$cmake_version-linux-x86_64.tar.gz"
tar -xzf "cmake-$cmake_version-linux-x86_64.tar.gz"
mv "cmake-$cmake_version-linux-x86_64" /opt/cmake
rm "cmake-$cmake_version-linux-x86_64.tar.gz"

echo "PATH=\"/opt/cmake/bin:$PATH\"" > /etc/environment

## ECM

echo "Install extra-cmake-modules"

git clone https://invent.kde.org/ahiemstra/extra-cmake-modules
mkdir extra-cmake-modules/build
pushd extra-cmake-modules/build

/opt/cmake/bin/cmake .. -DCMAKE_INSTALL_PREFIX=$prefix -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF
make install

popd

rm -r extra-cmake-modules
