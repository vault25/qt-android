FROM runmymind/docker-android-sdk:latest AS base

COPY package-list-minimal.txt /opt/tools/

RUN /opt/tools/entrypoint.sh built-in > /dev/null

RUN apt-get -yqq update \
    && apt-get -yqq --no-install-recommends install \
        build-essential \
        python3 \
        python3-requests \
        python3-xdg \
        python3-yaml \
        git \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

ENV ANDROID_NDK /opt/android-sdk-linux/ndk-bundle

CMD ["/bin/bash"]

### Build and install Qt

FROM base AS qt

COPY qt_android.sh /build/

WORKDIR /build

RUN /bin/bash /build/qt_android.sh 5.15.2

### Install other tools

FROM qt AS tools

ENV PATH /opt/cmake/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

COPY other_tools.sh /build/

WORKDIR /build

RUN /bin/bash /build/other_tools.sh
