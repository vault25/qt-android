#!/bin/bash

basedir=$(dirname "$(readlink -f "$0")")
log_file="/build/build.log"

prefix="/opt/libraries"

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 [qt version]"
    exit 1
fi

qt_version="$1"
qt_version_parts=(${qt_version//./ })
qt_maj_minor="${qt_version_parts[0]}.${qt_version_parts[1]}"

qt_base_name="qt"
qt_source_dir="$qt_base_name-$qt_version"

qt_abi="armeabi-v7a"

qt_modules=(
    "qtbase"
    "qtdeclarative"
    "qtsvg"
    "qtconnectivity"
    "qtmultimedia"
    "qtlocation"
    "qtsensors"
    "qtimageformats"
    "qtgraphicaleffects"
    "qtandroidextras"
    "qtquickcontrols2"
    "qtvirtualkeyboard"
    "qtnetworkauth"
)

openssl_dir="$prefix/openssl"
openssl_include_dir="$openssl_dir/include"
openssl_lib_dir="$openssl_dir/lib/multiabi"

if [ ! -d "$openssl_dir" ]; then
    # UGLY HACK!
    # Download a pre-built version of the openssl 1.1.1 libs since building
    # OpenSSL for android with a recent NDK is a major pain in the ass.
    echo "Downloading and extracting OpenSSL..."

    mkdir -p $openssl_dir
    pushd $openssl_dir

    wget -nv https://github.com/KDAB/android_openssl/archive/master.zip
    unzip -q master.zip

    mv android_openssl-master/static/* .

    rm -r android_openssl-master
    rm master.zip

    popd
fi

if [ ! -d "$qt_source_dir" ]; then
    git clone https://invent.kde.org/qt/qt/qt5 -b kde/5.15 $qt_source_dir
    pushd "$qt_source_dir"

    # Only build the specified modules, anything else is unused and will only
    # bloat the image.
    modules=${qt_modules[@]}
    ./init-repository --module-subset="${modules// /,}"
else
    pushd "$qt_source_dir"
fi

# Tell Qt where to find the OpenSSL libraries.
# This, along with the -I and -L below are required, otherwise OpenSSL will not
# be found and we won't have SSL support.
export OPENSSL_LIBS="-L$openssl_lib_dir -lcrypto_$qt_abi -lssl_$qt_abi"

echo "Configuring Qt..."
./configure \
    -prefix $prefix \
    -opensource \
    -confirm-license \
    -release \
    -xplatform android-clang \
    -android-ndk $ANDROID_SDK_ROOT/ndk-bundle \
    -android-sdk $ANDROID_SDK_ROOT \
    -android-ndk-host linux-x86_64 \
    -android-abis $qt_abi \
    -nomake tests \
    -nomake examples \
    -I$openssl_include_dir \
    -L$openssl_lib_dir \
    -openssl-linked \
    -no-widgets \
    -no-feature-geoservices_mapboxgl \
    -no-feature-geoservices_mapbox \
    -no-feature-ftp \
    -no-feature-pdf \
    -no-feature-printer \
    > $log_file

echo "Building Qt..."
make -j2 >> $log_file
make install >> $log_file

popd
rm -r "$qt_source_dir"

echo "Building QtMqtt"
git clone https://invent.kde.org/qt/qt/qtmqtt.git -b 5.15

pushd qtmqtt

$prefix/bin/qmake >> $log_file
make -j2 >> $log_file
make install

popd
rm -r qtmqtt
